//
//  MainMapVC.m
//  TestPaco
//
//  Created by Irvin Robles on 01/03/16.
//  Copyright © 2016 Irvin Robles. All rights reserved.
//

#import "MainMapVC.h"
#import <GoogleMaps/GoogleMaps.h>
#import "Constant.h"
#import "AFNetworking.h"
#import <Social/Social.h>

@interface MainMapVC ()
{
    CLLocationManager *locationManager;
    //Variable para obtener las predicciones de las direcciones al introducir texto en txtDireccionObjetivo
    GMSPlacesClient *placesClient;
    //Arreglo donde se almacenan las posibles predicciones del placesClient
    NSArray *searchPlaces;
    //Pin para el destino y el usuario
    GMSMarker *pinDestino;
    GMSMarker *pinUser;
    //Banderas que permiten el control del flujo de los estados de la app
    BOOL actionFlag;
    BOOL selectionFlag;
    BOOL insideFlag;
    BOOL inside1050;
    BOOL inside50100;
    BOOL inside100200;
    
}

@end

@implementation MainMapVC

- (void)viewDidLoad {
    [super viewDidLoad];
    //Iniciando la localizacion
    locationManager = [[CLLocationManager alloc] init];
    [locationManager startUpdatingLocation];
    //Asignando el delegate para poder obtener los cambios de posicion
    locationManager.delegate=self;
    //Configurando la precision del GPS
    locationManager.desiredAccuracy=kCLLocationAccuracyBest;
    locationManager.distanceFilter=kCLDistanceFilterNone;
    //Verificando si se tiene autorizacion par autilizar el GPS
    [self CheckForLocationAutorization];
    //Configurando para que se detecte cuando el usuario introduce una nueva letra
    [self.txtDireccionObjetivo addTarget:self
                        action:@selector(txtDireccionObjetivoDidChangeOrigen:)
              forControlEvents:UIControlEventEditingChanged];
    //Inicializando placesClient para poder autocompletar las direcciones
    placesClient=[[GMSPlacesClient alloc]init];
    self.txtDireccionObjetivo.delegate=self;
    actionFlag=NO;
    insideFlag=NO;
    inside50100=NO;
    inside1050=NO;
    inside100200=NO;
    selectionFlag=NO;
    self.mapView.delegate=self;
    
}

//Funcion que verifica si la app tiene permisos de localizacion, de no tenerlos, invita al usuario a activarlos
-(void)CheckForLocationAutorization
{
    //Verificando si esta disponible la localizacion
    if([CLLocationManager locationServicesEnabled])
    {
        //Verificando que tipo de permiso tiene la app para localizacion
        if([CLLocationManager authorizationStatus]==kCLAuthorizationStatusDenied || [CLLocationManager authorizationStatus] == kCLAuthorizationStatusRestricted)
        {
            //Enviando mensaje al usuario ya que no se tiene permiso para la localizacion
                UIAlertController *alert;
                
                alert =[UIAlertController
                        alertControllerWithTitle:@"TestPablo"
                        message:@"TestPablo no tiene permiso de localizacion, favor de habilitar la opcion desde Ajustes."
                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *accionSettings=[UIAlertAction actionWithTitle:@"Ajustes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
                    NSURL *appSettings = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
                    [[UIApplication sharedApplication] openURL:appSettings];
                }];
                [alert addAction:accionSettings];
                
                [self presentViewController:alert animated:YES completion:nil];
        }
        else
        {
            //Apartir de iOS 8 cambio la forma en como se pide el permiso, con esta validacion sabemos si el SO responde a esta autorizacion y ejecutarla.
            if ([locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
                [locationManager requestAlwaysAuthorization];
            }
            //Configurando para que se permita enviar localizacion en background
            locationManager.allowsBackgroundLocationUpdates=YES;
            [locationManager startUpdatingLocation];
            
            //Configurando la posicion del mapa con el pinUser en la posicion del usuario
            GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:locationManager.location.coordinate.latitude longitude:locationManager.location.coordinate.longitude zoom:16];
            self.mapView.camera=camera;
            [self.mapView clear];
            pinUser=[GMSMarker markerWithPosition:locationManager.location.coordinate];
            pinUser.icon=[UIImage imageNamed:@"pinUser"];
            pinUser.map=self.mapView;
        }
    }
}

//Funcion que detecta cuando se introdujo una nueva letra en le txtDireccionObjetivo
- (void)txtDireccionObjetivoDidChangeOrigen:(UITextField *)textfield {
    self.tblDirecciones.hidden=NO;
    //Configurando el radio y el centro para que la prediccion de las direcciones sean direcciones cercanas y no obtener direcciones de otros paises
    CLLocationCoordinate2D center = CLLocationCoordinate2DMake(locationManager.location.coordinate.latitude,locationManager.location.coordinate.longitude);
    CLLocationCoordinate2D northEast = CLLocationCoordinate2DMake(center.latitude + 0.001,
                                                                  center.longitude + 0.001);
    CLLocationCoordinate2D southWest = CLLocationCoordinate2DMake(center.latitude - 0.001,
                                                                  center.longitude - 0.001);
    //Configurando el filtro para las direcciones
    GMSAutocompleteFilter *filter = [[GMSAutocompleteFilter alloc] init];
    filter.type = kGMSPlacesAutocompleteTypeFilterNoFilter;
    
    GMSCoordinateBounds *radio=[[GMSCoordinateBounds alloc]init];
    radio=[radio includingCoordinate:center];
    radio=[radio includingCoordinate:northEast];
    radio=[radio includingCoordinate:southWest];
    
    //Evento que envia al servidor de google el texto y se reciben las posibles predicciones de las direcciones
    
    [placesClient autocompleteQuery:textfield.text bounds:radio filter:filter callback:^(NSArray *results, NSError *error) {
        if (error != nil) {
            return;
        }
        //Guardando las direcciones en el arreglo
        searchPlaces=results;
        //Actualizando los datos de la tabla de predicciones para que el usuario pueda seleccionar alguna
        [self.tblDirecciones reloadData];
    }];
}


//Evento que se dispara cuando hay un cambio en los permisos del gps para la app
- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus) status
{
    //Verificando que tipo de autorizacion es a la que se cambio
    if(status!=kCLAuthorizationStatusAuthorizedAlways && status!= kCLAuthorizationStatusNotDetermined)
    {
        //Enviando mensaje de que no se tienen los permisos de GPS correspondientes
        UIAlertController *alert;
        
        alert =[UIAlertController
                alertControllerWithTitle:@"TestPaco"
                message:@"TestPaco no tiene permiso de localizacion, favor de habilitar la opcion desde Ajustes."
                preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *accionSettings=[UIAlertAction actionWithTitle:@"Ajustes" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {
            NSURL *appSettings = [NSURL URLWithString:UIApplicationOpenSettingsURLString];
            [[UIApplication sharedApplication] openURL:appSettings];
        }];
        [alert addAction:accionSettings];
        
        [self presentViewController:alert animated:YES completion:nil];
    }
    else
    {
        //Apartir de iOS 8 cambio la forma en como se pide el permiso, con esta validacion sabemos si el SO responde a esta autorizacion y ejecutarla.
        if ([locationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) {
            [locationManager requestAlwaysAuthorization];
        }
        [locationManager startUpdatingLocation];
        locationManager.allowsBackgroundLocationUpdates=YES;
        //Configurando mapa con el pin del usuario en la posicion actual
        GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:locationManager.location.coordinate.latitude longitude:locationManager.location.coordinate.longitude zoom:16];
        self.mapView.camera=camera;
        pinUser=[GMSMarker markerWithPosition:locationManager.location.coordinate];
        pinUser.icon=[UIImage imageNamed:@"pinUser"];
        [self.mapView clear];
        pinUser.map=self.mapView;
    }
}


//Evento que se dispara al recibir una actualizacion de la ubicacion actual
- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    //Moviendo el pin a la nueva posicion
    pinUser.position=newLocation.coordinate;
    //verificando si se esta en camino al la direccion destino
    if (actionFlag) {
        //Calculando la distancia entre el usuario y el destino
        CLLocationDistance distance=[[[CLLocation alloc]initWithLatitude:pinUser.position.latitude longitude:pinUser.position.longitude] distanceFromLocation:[[CLLocation alloc] initWithLatitude:pinDestino.position.latitude longitude:pinDestino.position.longitude]];
        
        //Validando las distancias, mostrando letreros y enviando notificaciones correspondientes al flujo del camino
        if (distance>200) {
            self.txtDistancia.text=@"Estás muy lejos del punto objetivo";
        }
        else
        {
            if(distance>100 && distance<=200)
            {
                self.txtDistancia.text=@"Estas lejos del Punto Objectivo";
                insideFlag=NO;
                inside50100=NO;
                inside1050=NO;
                //Validando si no ya se estaba antes en este punto (para no mandar la notificacion muchas veces, solo cuando entra)
                if(!inside100200)
                {
                    //Enviando notificacion cuando al app esta en background
                    UILocalNotification* localNotification = [[UILocalNotification alloc] init];
                    localNotification.fireDate = [NSDate date];
                    localNotification.alertBody = @"Estas lejos del Punto Objectivo";
                    localNotification.soundName = UILocalNotificationDefaultSoundName;
                    localNotification.applicationIconBadgeNumber = 1;
                    
                    [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
                    inside100200=YES;
                }
            }
            else
            {
                if(distance>50 && distance <=100)
                {
                    self.txtDistancia.text=@"Estás próximo al punto objetivo";
                    insideFlag=NO;
                    inside100200=NO;
                    inside1050=NO;
                    //Validando si no ya se estaba antes en este punto (para no mandar la notificacion muchas veces, solo cuando entra)
                    if (!inside50100)
                    {
                        //Enviando notificacion cuando al app esta en background
                        UILocalNotification* localNotification = [[UILocalNotification alloc] init];
                        localNotification.fireDate = [NSDate date];
                        localNotification.alertBody = @"Estás próximo al punto objetivo";
                        localNotification.soundName = UILocalNotificationDefaultSoundName;
                        localNotification.applicationIconBadgeNumber = 1;
                        
                        [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
                        inside50100=YES;
                    }
                }
                else
                {
                    if(distance>10 && distance <=50)
                    {
                        self.txtDistancia.text=@"Estás muy próximo al punto objetivo";
                        insideFlag=NO;
                        inside100200=NO;
                        inside50100=NO;
                        //Validando si no ya se estaba antes en este punto (para no mandar la notificacion muchas veces, solo cuando entra)
                        if(!inside1050)
                        {
                            //Enviando notificacion cuando al app esta en background
                            UILocalNotification* localNotification = [[UILocalNotification alloc] init];
                            localNotification.fireDate = [NSDate date];
                            localNotification.alertBody = @"Estás muy próximo al punto objetivo";
                            localNotification.soundName = UILocalNotificationDefaultSoundName;
                            localNotification.applicationIconBadgeNumber = 1;
                            
                            [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];                        inside1050=YES;
                        }
                    }
                    else
                    {
                        //Validando si no ya se estaba antes en este punto (para no mandar la notificacion muchas veces, solo cuando entra)
                        inside50100=NO;
                        inside1050=NO;
                        inside100200=NO;
                        if(!insideFlag)
                        {
                            self.txtDistancia.text=@"Estás en el punto objetivo";
                            insideFlag=YES;
                            //Enviando notificacion cuando al app esta en background
                            UILocalNotification* localNotification = [[UILocalNotification alloc] init];
                            localNotification.fireDate = [NSDate date];
                            localNotification.alertBody = @"Estás en el punto objetivo";
                            localNotification.soundName = UILocalNotificationDefaultSoundName;
                            localNotification.applicationIconBadgeNumber = 1;
                            
                            [[UIApplication sharedApplication] scheduleLocalNotification:localNotification];
                            
                            //Mediante SOCIAL hago el post para twitter obteniendo la imagen del mapa y anexarlo al tweet
                            if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
                            {
                                SLComposeViewController *tweetSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
                                //Agregando el mensaje al tweet
                                [tweetSheet setInitialText:[NSString stringWithFormat:@"Latitud: %f, Longitud: %f",locationManager.location.coordinate.latitude,locationManager.location.coordinate.longitude]];
                                [tweetSheet addImage:[self imageWithView:self.mapView]];
                                
                                [self presentViewController:tweetSheet animated:YES completion:nil];
                            }
                            else
                            {
                                NSLog(@"No se pudo mandar el tweet");
                            }
                        }
                    }
                }
            }
        }
        
    }
}

//Evento que se dispara cuando se intenta editar la caja de texto
- (void) textFieldDidBeginEditing:(UITextField *)textField {
    //funcion para seleccionar todo el texto del txt y asi pode borrarlo si es lo que se quiere
    [textField performSelector:@selector(selectAll:) withObject:textField afterDelay:0.f];
}


//Configuracion de la tabla para los datos de la prediccion de direcciones

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(searchPlaces.count<1)
    {
        self.tblDirecciones.hidden=YES;
    }
    return searchPlaces.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    //Asignando el texto de la celda con la direccion
    GMSAutocompletePrediction *place=(GMSAutocompletePrediction *)searchPlaces[indexPath.row];
    cell.textLabel.text = place.attributedFullText.string;
    return cell;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSIndexPath *index = [NSIndexPath indexPathForRow:indexPath.row inSection:0];
    UITableViewCell *cell = (UITableViewCell *)[tableView cellForRowAtIndexPath:index];
    self.txtDireccionObjetivo.text= cell.textLabel.text;
    selectionFlag=YES;
    //Obteniendo el PlaceID de la celda seleccionada con el fin de poder obtener la coordenada de la direccion y asi poder colocar el pin en el mapa
    GMSAutocompletePrediction *place=(GMSAutocompletePrediction *)searchPlaces[indexPath.row];
    [self getCoordinateFromPlaceID:place.placeID];
    self.imgPin.hidden=YES;
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

//Funcion que obtiene las coordenadas de un PlaceID y lo pone en el mapa
-(void)getCoordinateFromPlaceID:(NSString *)placeID
{
    [placesClient lookUpPlaceID:placeID callback:^(GMSPlace *place, NSError *error) {
        if (error != nil) {
            NSLog(@"Place Details error %@", [error localizedDescription]);
            return;
        }
        if (place != nil) {
            
            //Verificando si el pinDestino ya tenia datos anteriores
            if(pinDestino!=nil)
            {
                //removiendo el pin del mapa
                pinDestino.map=nil;
            }
            //Asignando datos para el pinDestino en el mapa
            self.txtDireccionObjetivo.text=place.formattedAddress;
            pinDestino = [GMSMarker markerWithPosition:place.coordinate];
            pinDestino.title = self.txtDireccionObjetivo.text;
            pinDestino.icon=[UIImage imageNamed:@"pinDestino"];
            pinDestino.map=self.mapView;
            
            //Calculando distancia
            CLLocationDistance distance=[[[CLLocation alloc]initWithLatitude:pinUser.position.latitude longitude:pinUser.position.longitude] distanceFromLocation:[[CLLocation alloc] initWithLatitude:pinDestino.position.latitude longitude:pinDestino.position.longitude]];
            
            //verificando la distancia, con el fin de ajustar el mapa a a la distancia que hay entre el usuario y el destino y poder acomodar el zoom del mapa y se tenga una mejor experiencia de usuario
            if(distance>100)
            {
                GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] init];
                //pading para que los pin no sangan muy al borde de la pantalla
                UIEdgeInsets newPadding = UIEdgeInsetsMake(80, 80, 80, 80);
                bounds = [bounds includingCoordinate:pinDestino.position];
                bounds = [bounds includingCoordinate:pinUser.position];
                [self.mapView animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds withEdgeInsets:newPadding]];
            }
            else
            {
                //Si la distancia es muy corta, es suficiente colocar el zoom
                GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:locationManager.location.coordinate.latitude longitude:locationManager.location.coordinate.longitude zoom:16];
                self.mapView.camera=camera;
            }

            self.tblDirecciones.hidden=YES;
            [self.tblDirecciones reloadData];
            [self.txtDireccionObjetivo resignFirstResponder];
        }
    }];
}


//Boton que indica si se inicia o reinicia la funcion del mapa
- (IBAction)btnAccion:(id)sender
{
    //Verificando que accion esta en ejecucion
    if (!actionFlag)
    {
        //verificando si ya hay datos en el pin del destino
        if(pinDestino!=nil)
        {
            //Agregando los circulos con los radios establecidos
            CLLocationCoordinate2D circleCenter200 = pinDestino.position;
            GMSCircle *circ200 = [GMSCircle  circleWithPosition:circleCenter200 radius:200];
            circ200.fillColor = [UIColor colorWithRed:255/255.0 green:0/255.0 blue:0/255.0 alpha:1];
            circ200.strokeColor = [UIColor colorWithRed:255/255.0 green:0/255.0 blue:0/255.0 alpha:1];
            circ200.map = self.mapView;
            
            CLLocationCoordinate2D circleCenter100 = pinDestino.position;
            GMSCircle *circ100 = [GMSCircle  circleWithPosition:circleCenter100 radius:100];
            circ100.fillColor = [UIColor colorWithRed:240/255.0 green:125/255.0 blue:52/255.0 alpha:1];
            circ100.strokeColor = [UIColor colorWithRed:240/255.0 green:125/255.0 blue:21/255.0 alpha:1];
            circ100.map = self.mapView;
            
            CLLocationCoordinate2D circleCenter50 = pinDestino.position;
            GMSCircle *circ50 = [GMSCircle  circleWithPosition:circleCenter50 radius:50];
            circ50.fillColor = [UIColor colorWithRed:230/255.0 green:216/255.0 blue:101/255.0 alpha:1];
            circ50.strokeColor = [UIColor colorWithRed:230/255.0 green:216/255.0 blue:101/255.0 alpha:1];
            circ50.map = self.mapView;
            
            CLLocationCoordinate2D circleCenter10 = pinDestino.position;
            GMSCircle *circ10 = [GMSCircle  circleWithPosition:circleCenter10 radius:10];
            circ10.fillColor = [UIColor colorWithRed:87/255.0 green:207/255.0 blue:16/255.0 alpha:1];
            circ10.strokeColor = [UIColor colorWithRed:87/255.0 green:207/255.0 blue:16/255.0 alpha:1];
            circ10.map = self.mapView;
            
            actionFlag=YES;
            //Cambiando el titulo del boton para indicar la otra accion
            [self.btnAccion setTitle:@"Reiniciar" forState:UIControlStateNormal];
            //ocultando el pin que se encontraba en el centro del mapa
            self.imgPin.hidden=YES;
            self.txtDireccionObjetivo.userInteractionEnabled=NO;
            
            //Calculando distancia
            CLLocationDistance distance=[[[CLLocation alloc]initWithLatitude:pinUser.position.latitude longitude:pinUser.position.longitude] distanceFromLocation:[[CLLocation alloc] initWithLatitude:pinDestino.position.latitude longitude:pinDestino.position.longitude]];
            
            pinDestino.icon=[UIImage imageNamed:@"pinDestino"];
            pinDestino.map=self.mapView;
            
            //verificando la distancia, con el fin de ajustar el mapa a a la distancia que hay entre el usuario y el destino y poder acomodar el zoom del mapa y se tenga una mejor experiencia de usuario
            if(distance>100)
            {
                GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] init];
                //pading para que los pin no sangan muy al borde de la pantalla
                UIEdgeInsets newPadding = UIEdgeInsetsMake(80, 80, 80, 80);
                bounds = [bounds includingCoordinate:pinDestino.position];
                bounds = [bounds includingCoordinate:pinUser.position];
                [self.mapView animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds withEdgeInsets:newPadding]];
            }
            else
            {
                //Si la distancia es muy corta, es suficiente colocar el zoom
                GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:locationManager.location.coordinate.latitude longitude:locationManager.location.coordinate.longitude zoom:16];
                self.mapView.camera=camera;
            }
        }
    }
    else
    {
        //Regresando todos los valores a su estado inicial
        actionFlag=NO;
        insideFlag=NO;
        inside50100=NO;
        inside1050=NO;
        inside100200=NO;
        selectionFlag=NO;
        self.txtDireccionObjetivo.userInteractionEnabled=YES;
        self.imgPin.hidden=NO;
        [self.btnAccion setTitle:@"Iniciar" forState:UIControlStateNormal];
        self.txtDireccionObjetivo.text=@"";
        self.txtDistancia.text=@"";
        pinDestino.map=nil;
        pinDestino=nil;
        [self.mapView clear];
        GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:locationManager.location.coordinate.latitude longitude:locationManager.location.coordinate.longitude zoom:16];
        pinUser=[GMSMarker markerWithPosition:locationManager.location.coordinate];
        pinUser.map=self.mapView;
        pinUser.icon=[UIImage imageNamed:@"pinUser"];
        self.mapView.camera=camera;
    }
}

//Funcion que convierte de UIView a UIImage para obtener la imagen del mapa y poder agregarla al tweet

- (UIImage *) imageWithView:(UIView *)view
 {
     UIGraphicsBeginImageContextWithOptions(view.bounds.size, NO, 0.0);
     [view.layer renderInContext:UIGraphicsGetCurrentContext()];
     UIImage * img = UIGraphicsGetImageFromCurrentImageContext();
     UIGraphicsEndImageContext();
     return img;
 }

//Evento que se dispara cuando se DETIENE el movimiento del mapa

- (void)mapView:(GMSMapView *)mapView idleAtCameraPosition:(GMSCameraPosition *)position {
    if (!actionFlag)
    {
        //Verificando si la bandera de seleccion de direccion esta encendida para colocar el pin central
        if (!selectionFlag)
        {
            self.imgPin.hidden=NO;
            if(pinDestino!=nil)
            {
                pinDestino.map=nil;
                pinDestino=nil;
            }
            //obteniendo la direccion del punto en el mapa donde se dejo de mover
            GMSGeocoder *geocoder;
            geocoder=[[GMSGeocoder alloc]init];
            [geocoder reverseGeocodeCoordinate:position.target completionHandler:^(GMSReverseGeocodeResponse * handler, NSError *error) {
                if(error == nil)
                {
                    //Agregando la direccion de la coordenada a la caja de texto
                    NSString *direccion = [[NSString alloc]init];
                    if(handler.firstResult!=nil)
                    {
                        direccion=[NSString stringWithFormat:@"%@, %@",handler.firstResult.lines[0],handler.firstResult.lines[1]];
                    }
                    self.txtDireccionObjetivo.text=direccion;
                    self.txtDireccionObjetivo.textAlignment=NSTextAlignmentLeft;
                    pinDestino = [GMSMarker markerWithPosition:position.target];
                    pinDestino.title = self.txtDireccionObjetivo.text;
                    pinDestino.icon=[UIImage imageNamed:@"pinDestino"];
                }
            }];
        }
        else
        {
            selectionFlag=NO;
        }
    }
}

//Evento que se dispara cuando se hace un touch en el mapa
- (void)mapView:(GMSMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate
{
    //escondiendo el teclado y la escritura de la caja de texto
    [self.txtDireccionObjetivo resignFirstResponder];
    self.tblDirecciones.hidden=YES;
}


@end
